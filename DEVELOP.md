# Developer note

## Development tools

You can build OmegaT Textra plugin with Gradle building tool and IntelliJ IDEA IDE. 

## Report issue

Please put your issue on https://codeberg.org/miurahr/omegat-textra-plugin/issues issue tracker.

## Propose an improvement or a fix

You are recommended to register codeberg.org forge site and make a fork project on it.
Please feel free to raise Pull-Request on https://codeberg.org/miurahr/omegat-textra-plugin/pulls

